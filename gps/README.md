#GPS

The design contains 3 hierarchies- common, acq_hier and track_hier.
Content Description-

1. Common - a. comm_mem_read_ifc, comm_mem_write_ifc - manage memory for I,Q data for tracks
			b. Track_Status - monitors acquisition status of satellites 
			c. next_id - Decides which sat to be acquired next
			d. Control_SM_500_shifts - state machine for acquisition memory 

2. Acquire_Infrastructure - a. Acq_Mem_4_samples - Memory for incoming data. Parallel read. 8k samples
							b. Acq_Mem_ifc_500_shifts - manages writing to acquisition memory. Serial write to four memories.
							c. Acq_SM_500_shifts - manages reading from acquisition memory. gives reset to Acq_IP when not processing 	 data

3. Acq - a. Phase_Dop_IP_4_phases - gives out phases for each Doppler frequency
		 b. Cordic_Circ_Rot - Rotates the incoming data through Doppler_phase_angle
		 c. Adder - Gives all possible combinations of sum/difference of 4 samples
		 d. Prncode_chain_IP_500_shifts, Prncode_gen_IP_500_shifts, Count1_IP_500_shifts - locally generates PRN codes
		 e. Correlator_Bank_IP_xor - performs 500 correlations in parallel for shifted PRN codes. 4 additions in each cycle per 								correlator
		 f. Cordic_Circ_Vect - finds mag(I+iQ) from Correlator_Bank output
		 g. Count_IP_500_shifts - generates control signals like iteration change, freq change and block change
		 h. Sorter - sorts the values from Cordic_Circ_Vect
		 i. Presenting_Results - finds peak1/peak2 and outputs codephase, doppler_phase_shift   	

4. AD_interface - write rate from AD9361 IP is low, logic runs faster. Buffers data for a faster read

5. Tracking_Infrastructure - a. Buffer_Acq_Results - stores acquisition results of codephase and Doppler_phase_shift
							 b. FIFO_read_ifc, FIFO_write_ifc - reading and writing of data to track FIFOs
							 c. log_track - logs track data in FIFO for burst read
							 d. mux- selects whether data to be read from common memory or local FIFO
							 e. nav_bit_gen - generates navigation bits from track output
							 f. sipo_reg - serial in parallel out reg to store nav bits
							 g. Track - removes Doppler shift, calculates I_E,I_P,I_L,Q_E,Q_P,Q_L, implements DLL and PLL. outputs I_P 			  as track data 		 

6. ad9361_sw - files for setting parameters of AD9361, used by microblaze							 