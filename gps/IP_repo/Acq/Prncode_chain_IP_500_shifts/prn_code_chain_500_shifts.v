//Inputs
//1.strbe: Keep ticking
//2.cp_match : Log next iter stuff
//3.change_offset : Load new values of next iter
//4.code_reset : Load init values 
module prn_code_chain#
(parameter shifts_per_iter=500,
parameter adds_per_clk=4
)
(clk,reset, 
code_reset,change_offset,strbe,cp_match,id1,id2,
G1in1,G1in2,G1in3,G1in4,
G2in1,G2in2,G2in3,G2in4,
prn,
GA0,GA1,GA2,GA3,
GA248,GA249,GA256,GA257,
GB0,GB1,GB2,GB3,
GB248,GB249,GB256,GB257);
`include "init_params.vh"

input clk,reset;
input code_reset;
input change_offset,strbe;  //New state captured in prev iter will be loaded
input cp_match;             //For registering state for next set of shifts
input [3:0] id1,id2;        //For calculating prn code based on state

input [9:0] G1in1,G1in2,G1in3,G1in4;    //Next 4 States
input [9:0] G2in1,G2in2,G2in3,G2in4;    //Next 4 States

reg [9:0] G1_code [shifts_per_iter+adds_per_clk-1:0];        //Stores PRN code "states" of shifts_per_iter correlators   
reg [9:0] G2_code [shifts_per_iter+adds_per_clk-1:0];        //Also next 4 states
output wire [shifts_per_iter+adds_per_clk-1:0] prn;        

genvar k;
generate
    for(k=0; k<shifts_per_iter;k=k+1)
    begin:PRN
    localparam [9:0] GAinit= GA_INIT_TABLE[(10*(TABLE_N - k)-1)-:10];
    localparam [9:0] GBinit= GB_INIT_TABLE[(10*(TABLE_N - k)-1)-:10];

    reg [9:0] G1_next_iter,G2_next_iter;
    
        always @(posedge clk)
            if(reset || code_reset) 
            begin   //Initial States
                G1_code[k]<=GAinit;//init_statea[temp_addr];    
                G2_code[k]<=GBinit;//init_stateb[temp_addr];   
            end else if(change_offset) 
            begin   //Load Next iter state
                G1_code[k]<=G1_next_iter;    
                G2_code[k]<=G2_next_iter;   
            end else if(strbe) 
            begin   //Regular Tick
                G1_code[k]<=G1_code[k+adds_per_clk];    
                G2_code[k]<=G2_code[k+adds_per_clk];   
            end                            
        always @(posedge clk)
            if(cp_match) 
            begin   //Log next iter
            G1_next_iter<=G1_code[k];
            G2_next_iter<=G2_code[k];
            end                            

    //Code bits calculation
    wire [9:0] wr_G1,wr_G2;
        assign wr_G1=G1_code[k];
        assign wr_G2=G2_code[k];
        assign prn[k] = wr_G1[0]^wr_G2[id1]^wr_G2[id2]; 
    end                                    
endgenerate

reg rg_cp_match;
always @(posedge clk)
rg_cp_match<=cp_match;

genvar z;
generate
    for(z=0; z<adds_per_clk;z=z+1)
    begin:EPILOGUE
    localparam index=z+shifts_per_iter;
    reg [9:0] G1_next_iter,G2_next_iter;
    wire [9:0] GAin,GBin;

    assign GAin = (z==0)? G1in1:
                  (z==1)? G1in2:
                  (z==2)? G1in3:
                          G1in4;

    assign GBin = (z==0)? G2in1:
                  (z==1)? G2in2:
                  (z==2)? G2in3:
                          G2in4;
                                                   
    wire [9:0] GAinit,GBinit;
    assign GAinit = 10'h0C6;
    assign GBinit = 10'h2F4;                          
    
        always @(posedge clk)
            if(reset || code_reset) 
            begin   //Initial States
                G1_code[index]<=GAinit;    
                G2_code[index]<=GBinit;   
            end else if(change_offset) 
            begin   //Load Next iter state
                G1_code[index]<=G1_next_iter;    
                G2_code[index]<=G2_next_iter;   
            end else if(strbe) 
            begin   //Regular Tick
                G1_code[index]<=GAin;    
                G2_code[index]<=GBin;   
            end                            
        always @(posedge clk)
            if(cp_match) 
            begin   //Log next iter
            G1_next_iter<=G1_code[index];
            G2_next_iter<=G2_code[index];
            end                            

    //Code bits calculation
    wire [9:0] wr_G1,wr_G2;
        assign wr_G1=G1_code[index];
        assign wr_G2=G2_code[index];
        assign prn[index] = wr_G1[0]^wr_G2[id1]^wr_G2[id2]; 
    end                                    
endgenerate

////Debug
output wire [9:0] GA0,GA1,GA2,GA3;
output wire [9:0] GA248,GA249,GA256,GA257;
output wire [9:0] GB0,GB1,GB2,GB3;
output wire [9:0] GB248,GB249,GB256,GB257;
assign GA0=G1_code[0];
assign GA1=G1_code[1];
assign GA2=G1_code[2];
assign GA3=G1_code[3];
assign GA248=G1_code[248];
assign GA249=G1_code[249];
assign GA256=G1_code[256];
assign GA257=G1_code[257];
assign GB0=G2_code[0];
assign GB1=G2_code[1];
assign GB2=G2_code[2];
assign GB3=G2_code[3];
assign GB248=G2_code[248];
assign GB249=G2_code[249];
assign GB256=G2_code[256];
assign GB257=G2_code[257];



endmodule
