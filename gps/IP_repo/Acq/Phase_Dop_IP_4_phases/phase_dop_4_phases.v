
module Phase_Dop#
(parameter adds_per_clk=4,
parameter No_Freq=208,
parameter No_Freq_log2=8)
(clk,reset,strbe,ptr_change,iter_change,phase_per_sample,
phase1,phase2,phase3,phase4,freq_ptr,prn_change);
input clk,reset,strbe;
input iter_change;
input [31:0] phase_per_sample;
input ptr_change;
output reg [31:0] phase1;
output reg [31:0] phase2;
output reg [31:0] phase3;
output reg [31:0] phase4;
output reg [No_Freq_log2-1:0] freq_ptr;
output reg prn_change;

localparam adds_per_clk_log2=2;

always @(posedge clk)
if(reset || prn_change2)
freq_ptr<=0;
else if(ptr_change)
freq_ptr<=freq_ptr+1;

assign prn_change2 = ptr_change && (freq_ptr == No_Freq-1) ;

reg prn_change1;
always @(posedge clk)
begin
prn_change1<=prn_change2;
prn_change<=prn_change1;
end

reg [31:0] angle [adds_per_clk-1:0];
    
reg [adds_per_clk_log2:0] aaa;
	always @(posedge clk)
    begin
    if(reset || (iter_change)) begin
          angle[0]<=0;
          angle[1]<=phase_per_sample;
          angle[2]<=phase_per_sample+phase_per_sample;
          angle[3]<=phase_per_sample+phase_per_sample+phase_per_sample;
    end else if(strbe) begin
        for(aaa=0;aaa<adds_per_clk;aaa=aaa+1)
        begin
            angle[aaa]<=angle[aaa]+(phase_per_sample<<adds_per_clk_log2);
        end
    end
    end
    
    always @(posedge clk)
    begin
    phase1 <= angle[0];
    phase2 <= angle[1];
    phase3 <= angle[2];
    phase4 <= angle[3];
    end

endmodule
