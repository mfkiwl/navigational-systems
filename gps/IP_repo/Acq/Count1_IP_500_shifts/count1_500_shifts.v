`timescale 1ns / 1ps

module count1#
(parameter clks_per_iter_log2 = 10,
parameter no_iter_per_freq_log2 = 3,
parameter match = 125,// (shifts_per_iter/adds_per_clk)
parameter Num_Sat_log2=5)
(clk,reset,
control,prn_in,
strbe,code_reset,change_offset,cp_match,
id1,id2);

input clk,reset;
input [3:0] control;
input [5:0] prn_in;

//Unpacking control sinals
output strbe,code_reset,change_offset;
assign strbe = control[0];
assign code_reset = control[3];	   // next block
assign change_code = control[1];   // next sat
assign change_offset = control[2]; //iter_change

output cp_match;
output wire [3:0] id1,id2;

reg [clks_per_iter_log2-1:0] code_pointer;
reg [Num_Sat_log2:0] prn_no;
    
//Count of number of rotated samples coming into Correlators//
always @(posedge clk)
begin
    if(strbe)
        code_pointer<=code_pointer+1;
    else 
        code_pointer<=0;
end
assign cp_match=(code_pointer==match);
    

//To change tap points in accordance with Satellite number we're dealing with //
 
 always @(posedge clk)
 begin
     if(reset || (change_code))
         prn_no<=prn_in;
 end
 
wire [7:0] SATOUTS [1:32];
    
assign SATOUTS[ 1] = {4'd8,4'd4};
assign SATOUTS[ 2] = {4'd7,4'd3};
assign SATOUTS[ 3] = {4'd6,4'd2};
assign SATOUTS[ 4] = {4'd5,4'd1};
assign SATOUTS[ 5] = {4'd9,4'd1};
assign SATOUTS[ 6] = {4'd8,4'd0};
assign SATOUTS[ 7] = {4'd9,4'd2};
assign SATOUTS[ 8] = {4'd8,4'd1};
assign SATOUTS[ 9] = {4'd7,4'd0};
assign SATOUTS[10] = {4'd8,4'd7};
assign SATOUTS[11] = {4'd7,4'd6};
assign SATOUTS[12] = {4'd5,4'd4};
assign SATOUTS[13] = {4'd4,4'd3};
assign SATOUTS[14] = {4'd3,4'd2};
assign SATOUTS[15] = {4'd2,4'd1};
assign SATOUTS[16] = {4'd1,4'd0};
assign SATOUTS[17] = {4'd9,4'd6};
assign SATOUTS[18] = {4'd8,4'd5};
assign SATOUTS[19] = {4'd7,4'd4};
assign SATOUTS[20] = {4'd6,4'd3};
assign SATOUTS[21] = {4'd5,4'd2};
assign SATOUTS[22] = {4'd4,4'd1};
assign SATOUTS[23] = {4'd9,4'd7};
assign SATOUTS[24] = {4'd6,4'd4};
assign SATOUTS[25] = {4'd5,4'd3};
assign SATOUTS[26] = {4'd4,4'd2};
assign SATOUTS[27] = {4'd3,4'd1};
assign SATOUTS[28] = {4'd2,4'd0};
assign SATOUTS[29] = {4'd9,4'd4};
assign SATOUTS[30] = {4'd8,4'd3};
assign SATOUTS[31] = {4'd7,4'd2};
assign SATOUTS[32] = {4'd6,4'd1};
    
wire [7:0] OUT;

assign OUT = SATOUTS[prn_no];
assign id1 = OUT[3:0];
assign id2 = OUT[7:4];

endmodule
