/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 ////////////////////////////////////////////////////////////////////////////////////////
 // gedit:set shiftwidth=2 
 // -----------------------------------------------------------------------------
 // FILE NAME      : Cordic_Circ_Rot.v
 // DEPARTMENT     : RISE LAB
 // AUTHOR         : DAMARLA BALAJI,SYED M MD ZAID
 // AUTHOR'S EMAIL : ee15m033@ee.iitm.ac.in,ee15m039@ee.iitm.ac.in
 // -----------------------------------------------------------------------------
 // ----------------------------------------------------------------------------- 
 // This is a Modified code of cordic.v
 //*************************************************************************************/
 //  Title: Cordic.v
 //  Author:Claire Barnes
 //  Availability:https://github.com/cebarnes/cordic
 //*************************************************************************************/ 
 // Modifications:
 //1.Addition of log2(width) guard bits as suggested in
 //  J. S. Walther, "A unified algorithm for elementary functions," in
 //  Proceedings 38th Spring Joint Computer Conference, Atlantic City,
 //  New Jersey, 1971, pp. 379-385
 //2.Addition of Control signals which propagates through pipes along with 
 //  Inputs and this serves as strobe for outputs
 //3.Parameterization of 
 //  1.No of CORDIC Iterations
 //  2.Latency in CYCLES,ie.,No of Pipeline stages
 //4.Inputs are not Buffered thereby saving latency of One clockcycle
 //************************************************************************************/
 // Change history: Tuesday 24 January 2017 06:16:46 AM IST  - Version 1
 // Description : Given x,y it returns (x,y)*exp(j*theta)
 // Scaling-
 // x_out,y_out: 1.Due to padding of 'iter_num-width' number of zeros : 2^(output_width-(width+2))
 //              2.CORDIC circular mode scaling : ~1.6 (Product over sqrt(1+2^(-2j)) from 0 to iter_num-1)
 // 
 // Parameters-
 // 1.width - Inputs width
 // 2.iter_num -Number of cordic iterations required (Atleast > Inputs width)
 //             Output is guranteed to be correct upto 'iter_num' bits (excluding MSB overflow_guard)
 // 3.output_width - Required output_width (<(iter_num+overflow_guard))
 // 4.No_of_cycles - No of stages required. (MUST BE divisible factor of iter_num)
 // 5.frac_guard - Guard bits provided to avoid rounding errors (>log2(iter_num))
 // 6.overflow_guard - MSB bits account for CORDIC scaling(~1.6*sqrt(2)) 
 ///////////////////////////////////////////////////////////////////////////////
`define BUFFER_INPS
module CORDIC_CIRC_ROT# 
	(parameter width = 16,
	parameter iter_num = 16,
	parameter output_width=18,
	parameter No_of_cycles = 4,
	parameter frac_guard = 4,
	parameter iters_per_stage_log2 = 2,
	parameter overflow_guard = 2,
	parameter Piggyback_width=4)

	(clock, x_out, y_out, x_in, y_in, angle,stb_in,strbe);

	localparam iters_per_stage = (iter_num)/No_of_cycles;
	localparam intrmdte_wdth = overflow_guard+iter_num+frac_guard;
//	parameter iters_per_stage_log2 = $clog2(iters_per_stage);
	// Inputs
	input clock;
	//Coordinates
	input signed [width-1:0] x_in,y_in; 
	input signed [31:0] angle;
	//Control Piggyback Signals
	input [Piggyback_width-1:0] stb_in;
	//Outputs
	output signed  [output_width-1:0] x_out, y_out; 
	output [Piggyback_width-1:0] strbe;

    
    
//******************************************************************//
//Padding and extension of inputs to make intermediate wordlength   //
//capable of avoiding rounding errors till precison of iter_num bits//
//******************************************************************//
	wire signed [intrmdte_wdth-1:0] x_start,y_start;
	assign x_start ={{(overflow_guard){x_in[width-1]}},x_in,{(frac_guard+iter_num-width){1'b0}}};
	assign y_start ={{(overflow_guard){y_in[width-1]}},y_in,{(frac_guard+iter_num-width){1'b0}}};


//Pipeline registers to hold intermediate values //
//We have 'No_of_cycles' PIPE stages             //
	reg signed [intrmdte_wdth-1:0] x [0:No_of_cycles];
	reg signed [intrmdte_wdth-1:0] y [0:No_of_cycles];
	reg signed    [31:0] z [0:No_of_cycles];
//Register Passing control signals through PIPE //
	reg [Piggyback_width-1:0] strobe [0:No_of_cycles];

//******************************************************************//
//PRE-ROTATION : Make sure rotation angle is in -pi/2 to pi/2 range //
//Introduction of Pre rotated values into the CORDIC Pipe           //
//******************************************************************//
	wire [1:0] quadrant;
	assign quadrant = angle[31:30];
`ifdef BUFFER_INPS //Option to buffer ips costing an extra Cycle of Latency
	always @(posedge clock)
`else
	always @(*)
`endif
	begin // make sure the rotation angle is in the -pi/2 to pi/2 range
		case(quadrant)
		2'b00,
		2'b11: // no changes needed for these quadrants
		begin
				x[0] <= x_start;
				y[0] <= y_start;
				z[0] <= angle;
		end
		2'b01:// subtract pi/2 for angle in this quadrant
		begin
				x[0] <= -y_start;
				y[0] <= x_start;
				z[0] <= {2'b00,angle[29:0]}; 
		end
		2'b10:// add pi/2 to angles in this quadrant
		begin
				x[0] <= y_start;
				y[0] <= -x_start;
				z[0] <= {2'b11,angle[29:0]}; 
		end
	endcase
	end


`ifdef BUFFER_INPS //Option to buffer ips costing an extra Cycle of Latency
	always @(posedge clock)
`else
	always @(*)
`endif
	begin
		strobe[0]<= stb_in;
	end

  // run through iterations
	genvar i;
	generate
	for (i=0; i < No_of_cycles; i=i+1) //Block below is replicated 'No of stages' times
	begin: STAGES
		localparam integer j=i*iters_per_stage;
//All values are signed and hence perform signed operations such as Arith Shift
		wire signed [intrmdte_wdth-1:0] xi ; 
		wire signed [intrmdte_wdth-1:0] yi ;
		wire [31:0] zi;

		AddShift#
		(.intrmdte_wdth(intrmdte_wdth),
		 .iters_per_stage(iters_per_stage),
		 .j(j))
		U0
		(.xin(x[i]),.yin(y[i]),.zin(z[i]),
		 .xout(xi),.yout(yi),.zout(zi));
//Registering Intermediate Results for Pipelining the flow
		always @(posedge clock)
			begin
				x[i+1] <= xi;
				y[i+1] <= yi;
				z[i+1] <= zi;
				strobe[i+1] <= strobe[i];
			end 			
	end
	endgenerate

  

//MSB bits are sent out
	wire signed [intrmdte_wdth-1:0] sine,cosine;
	assign cosine = x[No_of_cycles];
	assign sine = y[No_of_cycles];

	assign x_out=cosine[(intrmdte_wdth-1)-:output_width];
	assign y_out=sine[(intrmdte_wdth-1)-:output_width];

	assign strbe =strobe[No_of_cycles];


endmodule





