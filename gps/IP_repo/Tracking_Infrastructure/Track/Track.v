/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 ////////////////////////////////////////////////////////////////////////////////////////
 // gedit:set shiftwidth=2 
 // -----------------------------------------------------------------------------
 // FILE NAME      : Track.v
 // DEPARTMENT     : RISE LAB
 // AUTHORS        : DAMARLA BALAJI,SYED M MD ZAID
 // AUTHORS' EMAIL : ee15m033@ee.iitm.ac.in,ee15m039@ee.iitm.ac.in
 // -----------------------------------------------------------------------------
 // ----------------------------------------------------------------------------- 
 // Algorithm is inspired from 
 //*************************************************************************************/
 //  Title: SoftGNSS v3.0
 //  Author: Darius Plausinaitis and Dennis M. Akos
 //  Date: 2006/08/14 12:08:03
 //  Code version: 1.1.2.12
 //  Availability: https://github.com/kristianpaul/SoftGNSS/blob/master/acquisition.m
 //
 //************************************************************************************/
 // Change history: Saturday 11 February 2017 03:47:48 AM IST   - Version 1
 // Description : This Module Tracks the GPS I,Q sample stream for its Doppler Frequency and Code Phase
 // 							It implements 
 // 							1.A Mixer(For Doppler Demodulation)- Implemented Using CORDIC CIRCULAR Mode Module in Vectoring mode
 // 							2.NCO - Since Code and Carrier Frequencies keep changing,They need to be updated,Which require Numerically
 // 											Controlled Oscillator
 // 							3.Three Correlators - Three Correlators for Early,Late And Prompt codes 
 // 							4.Loop Filter - This takes outputs of Correlators and Applies First Order Smoothing on them (PI)
 // 															Carrier Phase Error - Phase of Prompt Correlator atan(QP/IP)
 // 															Code    Phase Error - Offset Between Early and Late Correlators (|Early|-|Late|)/(|Early|+|Late|)
 // 							As a whole it acts as a First Order PLL

`define PI 3.14159265
`define SIM_N

//Testbench for Simulation

module Track#(
parameter input_width = 16,
parameter fs=4000, //In MHz
parameter blk_size_log2=11,
parameter code_length=1023,
parameter init_cp_per_sample = 1098437885)
(
input clk,
input reset,
input [31:0] Acquired_phase_per_sample,
input [11:0] Acquired_CodePhase,  
input [5:0] PRN_in,
input Acq_posedge,
input strobe,

input [input_width-1:0] Xin,
input [input_width-1:0] Yin,

input divisible,

output [29:0] I_P,
output [31:0] CodePhasePerSampleDEL,
output reg signed [31:0] CarrPhasePerSampleDEL,
output block_change,
output Loop_Comptns_Cmplte,
output Track_failed,
output [31:0] int_phi,
output [31:0] frac_phi,
output dummy_track_failed,
output reg [31:0] count,
output sign_change,
output div_check_start,
output div_by_four
);

reg [35:0] CLKID;
	always @(posedge clk)
	if(Acq_posedge||Track_failed)
		CLKID<=0;
	else if(Loop_Comptns_Cmplte) begin
//		$display("CLOCK - %d",CLKID);
		CLKID<=CLKID+1;
	end
assign Track_failed = CLKID[16];

// Code NCO,CodePhasePerSampleDEL is the feedback
	wire [2:0] code;
	reg [31:0] code_phase_per_sample;
	always @(posedge clk)
		if(Acq_posedge)
			code_phase_per_sample<=init_cp_per_sample;
		else if(Loop_Comptns_Cmplte)
			if(CodeSign)
			code_phase_per_sample<=init_cp_per_sample + CodePhasePerSampleDEL;
			else
			code_phase_per_sample<=init_cp_per_sample - CodePhasePerSampleDEL;
// Corrects code phase after every iteration by amount of CodePhasePerSampleDEL

reg [5:0] Acquired_PRN;
always @(posedge clk)
if(Acq_posedge)
Acquired_PRN<=PRN_in;

// CA code NCO,Generates Early,Late,Prompt Code
// Input is Code Phase Per Sample (inverse of frequency)
cacodeNCO#
(.phase_per_sample_width(32),
 .code_length_log2(10),
 .code_length(1023))
NCO
(.clk(clk),
 .reset(Acq_posedge),
 .strobe(strobe),
 .PRN(Acquired_PRN),
 .phase_per_sample(code_phase_per_sample), // Scalling is 2^(-32) (Unsigned)
 .O_EC(code[0]),.O_PC(code[1]),.O_LC(code[2]),
 .block_change(block_change),
 .cmltve_phse_int(int_phi),
 .cmltve_phse_frac(frac_phi));


// Code NCO,CodePhasePerSampleDEL is the feedback, 
reg signed [31:0] Doppler_phase_per_sample;
	always @(posedge clk)
		if(Acq_posedge)
			Doppler_phase_per_sample<=Acquired_phase_per_sample;
		else if(Loop_Comptns_Cmplte)
			Doppler_phase_per_sample<=Acquired_phase_per_sample - CarrPhasePerSampleDEL;

//This Provides input to CORDIC DIGITAL DOWN CONVERTER
reg signed [31:0] angle;
	always @(posedge clk)
		if(Acq_posedge)
			angle<=0;
		else if(strobe)
			angle<=angle+Doppler_phase_per_sample;

//Buffer everything
reg [2:0] code_in;
reg [31:0] phase;
reg stb_in;
always @(posedge clk)
begin
code_in <= code;
phase <= angle;
stb_in <= strobe;
end

wire [2:0] code_out;
// Word length = iter_rot +2
localparam op_wdth_rot =  input_width + 2;
wire [op_wdth_rot-1:0] COSout; //Output from CORDIC ROTOR
wire [op_wdth_rot-1:0] SINout;
// stb_out_negedge denotes end of block and this resets all Correlators
reg rg_blk_change;
	always @(posedge clk)
		rg_blk_change<=block_change;
		
//CORDIC DOWNCONVERSION OF INPUT STREAM BY ACQUIRED DOPPLER FREQUENCY
CORDIC_CIRC_ROT_1
 #(.width(input_width),//16
	.iter_num(16),//16
	.output_width(op_wdth_rot),//18
	.No_of_cycles (4),//6
	.frac_guard(4),//4
	.overflow_guard(2)) 
	ROTOR 
(   clk,
	code_in,
	code_out,
	COSout,
	SINout,
	Xin,
	Yin,
	phase,
	stb_in,
	stb_out,
	rg_blk_change,
	stb_out_negedge);

localparam crltn_sum_width = input_width + 2 + 12 ; //iter_rot + overflow_guard + log base 2 of samples_per_ms
// Sign Extension before Summation
wire [crltn_sum_width-1:0] COSout_SE,SINout_SE;
assign COSout_SE = {{(crltn_sum_width-op_wdth_rot){COSout[op_wdth_rot-1]}},COSout};
assign SINout_SE = {{(crltn_sum_width-op_wdth_rot){SINout[op_wdth_rot-1]}},SINout};




//-------------------------------------------------------------------------------------//
//Three Correlators
reg [crltn_sum_width-1:0] Acmltr_I [0:2];
reg [crltn_sum_width-1:0] Acmltr_Q [0:2];
reg [1:0] lv1;
	always @(posedge clk)
	begin
		if(Acq_posedge || stb_out_negedge) 
		begin
			for (lv1=0;lv1<3;lv1=lv1+1)
			begin
				Acmltr_I[lv1] <= 0;
				Acmltr_Q[lv1] <= 0;
			end
		end else
		if(stb_out)
		begin

			for (lv1=0;lv1<3;lv1=lv1+1)
			begin
				if(code_out[lv1]) begin
				Acmltr_I[lv1]<=Acmltr_I[lv1]+COSout_SE;Acmltr_Q[lv1]<=Acmltr_Q[lv1]+SINout_SE;
				end else begin
				Acmltr_I[lv1]<=Acmltr_I[lv1]-COSout_SE;Acmltr_Q[lv1]<=Acmltr_Q[lv1]-SINout_SE;
				end

			end
		end
	end
//-------------------------------------------------------------------------------------//

//Debug Purpose
//wire [crltn_sum_width-1:0] wire_Acmltr_I;
//assign wire_Acmltr_I=Acmltr_I[1];
//wire [crltn_sum_width-1:0] wr_Acmltr_I;
//assign wr_Acmltr_I=rg_Acmltr_I[1];
//Debug Purpose


//-------------------------------------------------------------//
//Correlator results are copied into registers for calculations
reg [crltn_sum_width-1:0] rg_Acmltr_I [0:3];
reg [crltn_sum_width-1:0] rg_Acmltr_Q [0:3];
reg [1:0] lv2;
	always @(posedge clk)
	begin
        if(stb_out_negedge) begin
			for (lv2=0;lv2<3;lv2=lv2+1)
			begin
				rg_Acmltr_I[lv2] <= Acmltr_I[lv2];
				rg_Acmltr_Q[lv2] <= Acmltr_Q[lv2];
			end
		end
	end
//-------------------------------------------------------------//
assign I_P=rg_Acmltr_I[1];

reg [2:0] Mag_stb_in;
wire [2:0] Mag_stb_out;
		always@(posedge clk)
		if(Acq_posedge)
				Mag_stb_in[0]<=0;
		else
				Mag_stb_in[0]<=stb_out_negedge;

genvar ii;
	genvar i;
	generate
	for (ii=0; ii < 2; ii=ii+1) 
	begin: STB_MAG // One Hot encoding representing ID of value inside Cart2Pol module
		always@(posedge clk)
		if(Acq_posedge)
				Mag_stb_in[ii+1]<=0;
		else
				Mag_stb_in[ii+1]<=Mag_stb_in[ii];
	end
	endgenerate


wire [crltn_sum_width-1:0] In1,In2;
assign In1 = Mag_stb_in[0] ? rg_Acmltr_I[0] :    // LATE
						 Mag_stb_in[1] ? rg_Acmltr_I[2] :    // EARLY
						 Mag_stb_in[2] ? rg_Acmltr_I[1] : 0; // PROMPT
assign In2 = Mag_stb_in[0] ? rg_Acmltr_Q[0] :
						 Mag_stb_in[1] ? rg_Acmltr_Q[2] :
						 Mag_stb_in[2] ? rg_Acmltr_Q[1] : 0;

localparam op_wdth_mag = crltn_sum_width + 2 ; // 
wire signed [op_wdth_mag-1:0] Mag_Out;
wire signed [31:0] Phase;


CORDIC_CIRC_VECT_1#
(.width(crltn_sum_width), //30
 .iter_num(crltn_sum_width), //30
 .output_width(op_wdth_mag), //32
 .No_of_cycles(10), //n is factor of (iter) ie.,30
 .frac_guard(5),
 .overflow_guard(2),
 .Piggyback_Cntrl_wdth(3))
CART2POL
(.clock(clk),
 .x_in(In1),
 .y_in(In2),
 .Mag_Out(Mag_Out),
 .Phase(Phase),
 .stb_in(Mag_stb_in),
 .stb_out(Mag_stb_out));

reg [op_wdth_mag-1:0] rg_Mag_Out;
	always @(posedge clk)
	rg_Mag_Out<=Mag_Out;

wire signed [op_wdth_mag-1:0] Sum,Diff;
assign Sum=Mag_Out+rg_Mag_Out;
assign Diff=Mag_Out-rg_Mag_Out;
assign Div_stb_in = (Mag_stb_out[1]==1) ? 1:0;


parameter code_err_wdth=32;
parameter code_err_iter=code_err_wdth;
wire [code_err_wdth-1:0] ERR_CODE;// 2^(-31) Scaling

//Division----------------------------------------------//
CORDIC_LIN_VECT#
(.width(op_wdth_mag),
 .iter_num(code_err_wdth),
 .output_width(code_err_iter),
 .No_of_cycles(8),
 .frac_guard(5),
 .Piggyback_Cntrl_wdth(1))
DIV
(clk,Sum,Diff,ERR_CODE,Div_stb_in,Div_stb_out);

reg signed [31:0] CarrError,CarrErrorDel;// 2^(-31) Scaling
	always @(posedge clk)
	begin
		if(Acq_posedge)
		begin
			CarrError<=0;
		end else if(Mag_stb_out[2])
		begin
			CarrError<=(Phase>>>1);
			CarrErrorDel<=(Phase>>>1)-CarrError;
		end
	end

reg CarrErr_stb1,CarrErr_stb2;
	always @(posedge clk)
	begin
		CarrErr_stb1<=Mag_stb_out[2];
		CarrErr_stb2<=CarrErr_stb1;
	end

reg [31:0] CodeError,CodeErrorDel;
	always @(posedge clk)
	begin
		if(Acq_posedge)
		begin
			CodeError<=0;
		end else if(Div_stb_out)
		begin
			CodeError<=ERR_CODE;
			CodeErrorDel<=ERR_CODE-CodeError;
		end
	end

reg rg_Div_stb_out,rg_rg_Div_stb_out;
	always @(posedge clk)
	begin
		rg_Div_stb_out<=Div_stb_out;
		rg_rg_Div_stb_out<=rg_Div_stb_out;
	end

//Scaling
//1.(tau2carr/tau1carr)=662.162162162162; (Multiplier ip range [-1,1],so Scale1:2^(-10))
//  Represent it in 32 bit number = 32'h52C5306E
//2.(PDIcarr/tau1carr)=22.3703433162893 (Multiplier ip range [-1,1],so Scale1:2^(-5))
//  Represent it in 32 bit number = 32'h597B3B47
//THE FIXED POINT IN RESULT OF MULTIPLICATION IS ONE BIT LEFT TO MSB DUE TO ADDITIONAL 2^-10 
//SCALING,FIXED POINT OF (tau2carr/tau1carr)*CarrErrorDel IS 11 BITS FROM MSB SIDE
//AND FIXED POINT OF (PDIcarr/tau1carr)*CarrError IS ALSO MOVED TO 11 BITS FROM MSB SIDE BY
//RIGHT SHIFT '5 BITS'ie.,(10-5) 
/*****************************************************************************************/
//1.(tau2code/tau1code)=52.972972972973; (Multiplier ip range [-1,1],so Scale1:2^(-6))
//  Represent it in 32 bit number = 32'h69F22983
//2.(PDIcode/tau1code)=0.143170197224251 (Multiplier ip range [-1,1],so Scale1:2^(0))
//  Represent it in 32 bit number = 32'h125366A9
//THE FIXED POINT IN RESULT OF MULTIPLICATION IS ONE BIT LEFT TO MSB DUE TO ADDITIONAL 2^-6 
//SCALING,FIXED POINT OF (tau2code/tau1code)*CodeErrorDel IS 7 BITS FROM MSB SIDE
//AND FIXED POINT OF (PDIcode/tau1code)*CodeError IS ALSO MOVED TO 7 BITS FROM MSB SIDE BY
//RIGHT SHIFT '6 BITS'ie.,(6-0) 
/*****************************************************************************************/
reg [31:0] Mul_InX,Mul_InY;
reg [2:0] Mul_stb_in;
	always @(posedge clk)
	begin
			if(CarrErr_stb1)
			begin
				Mul_InX<=CarrError;
				Mul_InY<=32'h597B3B47;
				Mul_stb_in<=1;
			end else if(CarrErr_stb2)
			begin
				Mul_InX<=CarrErrorDel;
				Mul_InY<=32'h52C5306E;
				Mul_stb_in<=2;
			end else if(rg_Div_stb_out)
			begin
				Mul_InX<=CodeError;
				Mul_InY<=32'h125366A9;
				Mul_stb_in<=3;
			end else if(rg_rg_Div_stb_out)
			begin
				Mul_InX<=CodeErrorDel;
				Mul_InY<=32'h69F22983;
				Mul_stb_in<=4;
			end else if(Carr_NCO_Freq_Cmplte)
			begin
				Mul_stb_in<=5;
				Mul_InX<=CarrNCO;
				Mul_InY<=(32'h00218DEF)>>>1; //(2/fs)*(2^(31-No of Frac Bits in CarrNCO))*2^31
			end else if(Code_NCO_Freq_Cmplte)
			begin
				Mul_stb_in<=6;
				Mul_InX<=CodeNCO;
				Mul_InY<=(32'h00010C6F)>>>1;//(1/fs)*(2^(31-No of Frac Bits in CarrNCO))*2^32
			end else
				Mul_stb_in<=0;
	end
reg signed [31:0] CarrNCO; // Q11.21 11 bits for int 21 for fraction
	always @(posedge clk)
	begin
		if(Acq_posedge)
			CarrNCO<=0;
		else if((Mul_stb_out==1)) 
			CarrNCO<=CarrNCO+(Mul>>>5);
		else if((Mul_stb_out==2))
			CarrNCO<=CarrNCO+Mul;
	end

reg signed [31:0] CodeNCO; // Q7.25 7 bits for int 25 for fraction
	always @(posedge clk)
	begin
		if(Acq_posedge)
			CodeNCO<=0;
		else if((Mul_stb_out==3)) 
			CodeNCO<=CodeNCO+(Mul>>>6);
		else if((Mul_stb_out==4))
			CodeNCO<=CodeNCO+Mul;
	end
reg Code_NCO_Freq_Cmplte;
always @(posedge clk)
	Code_NCO_Freq_Cmplte <=(Mul_stb_out==4);

reg Carr_NCO_Freq_Cmplte;
always @(posedge clk)
	Carr_NCO_Freq_Cmplte <=(Mul_stb_out==2);

reg Loop_Comptns_Cmplte;
always @(posedge clk)
	Loop_Comptns_Cmplte <=(Mul_stb_out==6);

always @(posedge clk)
    if(Mul_stb_out==5)
		CarrPhasePerSampleDEL<=Mul;

reg signed [31:0] CodePhasePerSampleTmp;
always @(posedge clk)
	if(Mul_stb_out==6)
		CodePhasePerSampleTmp<=Mul;


assign CodePhasePerSampleDEL = CodePhasePerSampleTmp[31] ? (-CodePhasePerSampleTmp)<<<1 : CodePhasePerSampleTmp<<<1 ;
assign CodeSign = CodePhasePerSampleTmp[31];

//Multiplier----------------------------------------------//
wire signed [31:0] Mul;
wire [2:0] Mul_stb_out;
CORDIC_LIN_ROT#
(.width(32),
 .iter_num(32),
 .output_width(32),
 .No_of_cycles(8),
 .frac_guard(5),
 .Piggyback_Cntrl_wdth(3))
MUL
(clk,Mul_InX,Mul_InY,Mul,Mul_stb_in,Mul_stb_out);

//-------------------------------------------------------------------------------------//
//Trial Track Failure
reg I_P_sign;
reg rg_I_P_sign;

always @(posedge clk)
begin
	if(stb_out_negedge)
		I_P_sign <= Acmltr_I[1][29];
end

always @(posedge clk)
begin
	rg_I_P_sign <= I_P_sign;
end

assign sign_change = I_P_sign!=rg_I_P_sign;

always @(posedge clk)
begin		
	if(sign_change || reset)
		count<=0;
	else if(~sign_change && stb_out_negedge)
		count <= count+1;
end

assign div_by_four = (count[0]==0 && count[1]==0);
assign div_check_start = (sign_change && (count>19) && div_by_four);
assign dummy_track_failed = ~(div_check_start && divisible);


endmodule
