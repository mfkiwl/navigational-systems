`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2017 07:42:01 PM
// Design Name: 
// Module Name: log_track_data_SM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module log_track_data_SM#
(parameter count=40000)
(clk,Data_count,Empty,
State);


input clk,Empty;
input [15:0] Data_count;
output reg State;

initial State=0;
always@ (posedge clk)
case(State)
0:
if (Data_count==count)
State<=1;
1:
if(Empty)
State<=0;
endcase


endmodule
