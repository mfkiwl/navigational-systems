`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////  
// Create Date: 19.03.2018 19:35:06
// Module Name: nav_bit_gen
// Description: This module gets the nav bit data from track output. Each 20 continuous
//              levels of track output represent one nav bit.
//////////////////////////////////////////////////////////////////////////////////


module nav_bit_gen#
(parameter quant_level = 1073741824) //(2^30)
(clk,reset,I_P,
state, nav_bit, nav_valid, track_failed, 
nav_state, first_transition, bit_transition, count, next_nav, reg_track_bit, track_bit,zero_to_one //debug
);

input clk, reset, state;
input [29:0] I_P;
output nav_valid, track_failed;
output nav_bit;
//debug
output reg nav_state;
output first_transition, bit_transition, next_nav, track_bit;
output reg [4:0] count;
output reg reg_track_bit;

//wire first_transition, bit_transition;
reg [4:0] count;

//state machine to get first transition
//reg nav_state;
always @(posedge clk) begin
	if (reset)
		nav_state <= 0;		//idle state
	else begin
	case(nav_state)
	0: if(first_transition)
		nav_state <= 1;
	endcase	
	end
end

// count till 20
always @(posedge clk) begin
 	if (reset || bit_transition || next_nav)
 		count <= 0;
 	else if (~bit_transition && state==1) begin
 		count <= count + 1;
 	end
 end

// determine track_bit level
//wire track_bit;
assign track_bit = I_P[29];

//reg reg_track_bit;
// determine bit transition
always @(posedge clk) begin
	reg_track_bit <= track_bit;
end 

// make sure the first transition is not from 0 to 1
//at reset track_bit will be 0, if first track bit at state==1 is 1,false first_transition signal is raised
output wire zero_to_one;
assign zero_to_one = track_bit && ~reg_track_bit;

// transition signals
wire next_nav;
assign bit_transition = state==1 && (reg_track_bit != track_bit);
assign first_transition = nav_state==0 && bit_transition && ~zero_to_one;
assign track_failed = nav_state == 1 && bit_transition && count<19;
assign next_nav = count == 19;

// nav_bit should be read only at nav_valid
assign nav_valid = next_nav && nav_state==1 && ~track_failed;
assign nav_bit = reg_track_bit;


endmodule