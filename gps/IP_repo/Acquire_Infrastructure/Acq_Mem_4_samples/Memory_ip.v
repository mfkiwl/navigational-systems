`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/15/2017 11:30:21 PM
// Design Name: 
// Module Name: Memory_ip
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memory_ip
(
clka,clkb,
wea1,wea2,wea3,wea4,
addra,dinI,dinQ,ena,
addrb,
I1,I2,I3,I4,
Q1,Q2,Q3,Q4
);

input wea1,wea2,wea3,wea4;
input clka;
input ena;
input [11:0] addra;
input [15:0] dinI;
input [15:0] dinQ;


input clkb;
input [11:0] addrb;
output [15:0] I1,I2,I3,I4;
output [15:0] Q1,Q2,Q3,Q4;
assign web=1'b0;
blk_mem_gen_0 MEM_I1 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea1),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinI),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(I1)  // output wire [15 : 0] doutb
);
blk_mem_gen_0 MEM_I2 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea2),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinI),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(I2)  // output wire [15 : 0] doutb
);

blk_mem_gen_0 MEM_I3 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea3),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinI),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(I3)  // output wire [15 : 0] doutb
);

blk_mem_gen_0 MEM_I4 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea4),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinI),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(I4)  // output wire [15 : 0] doutb
);

blk_mem_gen_0 MEM_Q1 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea1),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinQ),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(Q1)  // output wire [15 : 0] doutb
);

blk_mem_gen_0 MEM_Q2 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea2),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinQ),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(Q2)  // output wire [15 : 0] doutb
);

blk_mem_gen_0 MEM_Q3 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea3),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinQ),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(Q3)  // output wire [15 : 0] doutb
);
blk_mem_gen_0 MEM_Q4 (
  .clka(clka),    // input wire clka
  .ena(ena),      // input wire ena
  .wea(wea4),      // input wire [0 : 0] wea
  .addra(addra),  // input wire [11 : 0] addra
  .dina(dinQ),    // input wire [15 : 0] dina
  .douta(),  // output wire [15 : 0] douta
  .clkb(clkb),    // input wire clkb
  .web(web),      // input wire [0 : 0] web
  .addrb(addrb),  // input wire [11 : 0] addrb
  .dinb(),    // input wire [15 : 0] dinb
  .doutb(Q4)  // output wire [15 : 0] doutb
);

endmodule
