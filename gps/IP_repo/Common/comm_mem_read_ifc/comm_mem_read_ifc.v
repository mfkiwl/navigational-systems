`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 21.06.2017 15:45:35
// Module Name: comm_mem_read_ifc
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module com_mem_read(
clk,reset,ce,
//From Acq
SAT_complete,SAT_Acquired,SAT_Codephase,
//From Common_Write
id_track,wr_ptr,
start_ptr0,start_ptr1,start_ptr2,start_ptr3,
samples_rem0,samples_rem1,samples_rem2,samples_rem3,
//From Track Modules
BUSY0,BUSY1,BUSY2,BUSY3,
RDY0,RDY1,RDY2,RDY3,
Sat_Status0,Sat_Status1,Sat_Status2,Sat_Status3,
//To Memory
rd_ptr,
//To Track
stb,
//Debug
state,stop,id_read,samples_rem,stop_ptr_valid,stop_ptr,BUSY,RDY
);
input clk,reset,ce;

input SAT_complete,SAT_Acquired;
input [11:0] SAT_Codephase;

input [1:0] id_track;

input BUSY0,BUSY1,BUSY2,BUSY3;
input RDY0,RDY1,RDY2,RDY3;
input Sat_Status0,Sat_Status1,Sat_Status2,Sat_Status3;

input [17:0] start_ptr0,start_ptr1,start_ptr2,start_ptr3;
input [17:0] samples_rem0,samples_rem1,samples_rem2,samples_rem3;


input [17:0] wr_ptr;
output [17:0] rd_ptr;
output stb;

//Debug
output  [1:0] state;
output stop,stop_ptr_valid,BUSY,RDY,Sat_Status;
output  [1:0] id_read;
output  [17:0] samples_rem,stop_ptr;


localparam samples_per_ms=4000;


reg [1:0] state;
reg [11:0] cp;
reg [1:0] id_read;
always @(posedge clk)
begin
	if(reset)
		state <=0;
	else begin
	case(state)
	0: //Idle , Wait for SAT_complete,buffer cp 
	if(SAT_complete)begin
		if(SAT_Acquired)
		state<=1;
		else
		state<=0;

		cp<=SAT_Codephase;
		id_read <= id_track;
	end
	1: //Wait state for loading of read_pointer to correct value.
		state<=2;
	2:
	if(stop || ~Sat_Status)
		state<=0;
	else if(BUSY)
		state<=3;
	3:
	if(RDY) //Wait state for Track 
		state<=2;
	endcase
	end
end

reg [17:0] rd_ptr;
always @(posedge clk) begin
	if(state == 1)
		rd_ptr <= start_ptr + samples_per_ms - cp;
	else if (state == 2)
		rd_ptr <= rd_ptr+1;
end
assign stop = rd_ptr==stop_ptr && stop_ptr_valid;
assign stb = state==2 ; 


//When 9(Arbitarily chosen) samples are remaining
reg [17:0] stop_ptr;
always @(posedge clk)begin
if(samples_rem == 4000)
stop_ptr <= ce? wr_ptr:wr_ptr-1; 
end

reg BUSY, RDY;
reg [17:0] start_ptr,samples_rem;
reg Sat_Status;
//Based on id_read(Track id we are serving now) 
//BUSY,RDY,start_ptr,samples_rem get their values
always @(*)
begin
	case (id_read)
	0:begin
		BUSY = BUSY0;
		RDY = RDY0;
		start_ptr = start_ptr0;
		samples_rem = samples_rem0;
		Sat_Status = Sat_Status0;
	end
	1:begin
		BUSY = BUSY1;
		RDY = RDY1;
		start_ptr = start_ptr1;
		samples_rem = samples_rem1;
		Sat_Status = Sat_Status1;
	end
	2:begin
		BUSY = BUSY2;
		RDY = RDY2;
		start_ptr = start_ptr2;
		samples_rem = samples_rem2;
		Sat_Status = Sat_Status2;
	end
	3:begin
		BUSY = BUSY3;
		RDY = RDY3;
		start_ptr = start_ptr3;
		samples_rem = samples_rem3;
		Sat_Status = Sat_Status3;
	end
	endcase
end
// stop_ptr_valid  is high from instant samples_rem becomes 9 till READ_SM comes to IDLE
reg stop_ptr_valid;
always @(posedge clk)
begin
	if(reset||state==0)
		stop_ptr_valid<=0;
	else if(state!=0 && samples_rem==4000)
		stop_ptr_valid<=1;
end

endmodule
