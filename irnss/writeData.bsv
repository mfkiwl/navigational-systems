package writeData;
	import initial_Settings::*;
	import BRAMCore::*;
	import Vector::*;

	interface WriteIFC;
		method Action readData(Int#(BitSize) i1, Int#(BitSize) q1);
		method Bit#(1) returnComplete();
	endinterface

	module mkWrite();
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_I1 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_I2 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_I3 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_I4 <- mkBRAMCore1(3000,False);

		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_Q1 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_Q2 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_Q3 <- mkBRAMCore1(3000,False);
		BRAM_PORT#(Bit#(13),Reg#(Int#(BitSize))) signal_Q4 <- mkBRAMCore1(3000,False);

		Reg#(Int#(BitSize)) i <- mkReg(0);
		Reg#(Int#(BitSize)) q <- mkReg(0);

		Reg#(Int#(15)) count <- mkReg(0);
		Reg#(Bit#(1)) flag <- mkReg(0);
		Reg#(Bit#(13)) index <- mkReg(0);

		rule write(count < limit && flag == 1);
			if(count % 4 == 1)
			begin
				signal_I1.put(True,index,i);
				signal_Q1.put(True,index,q);
			end

			else if(count % 4 == 2)
			begin
				signal_I2.put(True,index,i);
				signal_Q2.put(True,index,q);
			end
			
			else if(count % 4 == 3)
			begin
				signal_I3.put(True,index,i);
				signal_Q3.put(True,index,q);
			end
			
			else if(count % 4 == 0)
			begin
				signal_I4.put(True,index,i);
				signal_Q4.put(True,index,q);
			end

			count <= count + 1;
			index <= index + 4;

			flag <= 0;
		endrule

		method Action readData(Int#(BitSize) i1,Int#(BitSize) q1) if(flag == 0);
			i <= i1;
			q <= q1;
			flag <= 1;
		endmethod

		method Bit#(1) returnComplete() if(count >= limit);
			return 1;
		endmethod

	endmodule // mkWrite
endpackage