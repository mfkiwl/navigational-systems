package correlator;
	import initial_Settings::*;
	import ConfigReg::*;
	import Vector::*;
	import FixedPoint::*;
	import Real::*;
	import BRAMCore::*;
	
	

	interface Corr_ifc;
		method Action readData(Int#(BitSize) c1,Int#(BitSize) c2,Int#(BitSize) c3,Int#(BitSize) c4,Int#(BitSize) yi1,Int#(BitSize) yi2,Int#(BitSize) yi3,Int#(BitSize) yi4);
		method Int#(SumSize) results();
	endinterface
	
	module mkCorr(Corr_ifc);

		Reg#(Bit#(2)) flag <- mkConfigReg(0);
		Reg#(Bit#(1)) readFlag <- mkReg(0);

		Reg#(Int#(ProdSize)) lv_prod_1 <- mkReg(1);
		Reg#(Int#(ProdSize)) lv_prod_2 <- mkReg(1);
		Reg#(Int#(ProdSize)) lv_prod_3 <- mkReg(1);
		Reg#(Int#(ProdSize)) lv_prod_4 <- mkReg(1);

		Reg#(Int#(BitSize)) code_1 <- mkReg(0);
		Reg#(Int#(BitSize)) code_2 <- mkReg(0);
		Reg#(Int#(BitSize)) code_3 <- mkReg(0);
		Reg#(Int#(BitSize)) code_4 <- mkReg(0);
		Reg#(Int#(BitSize)) yi_1 <- mkReg(0);
		Reg#(Int#(BitSize)) yi_2 <- mkReg(0);
		Reg#(Int#(BitSize)) yi_3 <- mkReg(0);
		Reg#(Int#(BitSize)) yi_4 <- mkReg(0);
		Reg#(Int#(SumSize)) sum <- mkConfigReg(0);

		Reg#(Int#(BitSize)) count <- mkReg(0);
		
		rule r1 (readFlag == 1 &&  flag == 1);
			//$display("Multiplying...");
			lv_prod_1 <= signExtend(code_1) * signExtend(yi_1);//code1[count] * yi[count];
			lv_prod_2 <= signExtend(code_2) * signExtend(yi_2);//code1[count+1] * yi[count+1];
			lv_prod_3 <= signExtend(code_3) * signExtend(yi_3);//code1[count+2] * yi[count+2];
			lv_prod_4 <= signExtend(code_4) * signExtend(yi_4);//code1[count+3] * yi[count+3];
			//$display("%d,%d,%d,%d",code_1,code_2,code_3,code_4);
			//$display("%d,%d,%d,%d",yi_1,yi_2,yi_3,yi_4);
			//$display("Check 1");
			flag <= 2;
			//$display("%d",flag);
		endrule
		
		rule r2 (flag == 2);
			//$display("%d,%d,%d,%d",lv_prod_1,lv_prod_2,lv_prod_3,lv_prod_4);
			sum <= signExtend(lv_prod_1) + signExtend(lv_prod_2) + signExtend(lv_prod_3) + signExtend(lv_prod_4);
			//sum <= (sum*sum) / (limit*limit);
			//count <= count+4;
			//$display("Check 2");
			flag <=0;
		endrule

		method Int#(SumSize) results() if(flag==0);// if (count >= limit);
			return sum;
		endmethod

		method Action readData(Int#(BitSize) c1,Int#(BitSize) c2,Int#(BitSize) c3,Int#(BitSize) c4,Int#(BitSize) yi1,Int#(BitSize) yi2,Int#(BitSize) yi3,Int#(BitSize) yi4) if(flag==0);
			code_1 <= c1;
			code_2 <= c2;
			code_3 <= c3;
			code_4 <= c4;
		 	yi_1 <= yi1;
			yi_2 <= yi2;
		 	yi_3 <= yi3;
		 	yi_4 <= yi4;

			//Reg#() sum <- mkReg(sum1);
			//sum <= sum1;
			readFlag <= 1;
			flag <=1;

			//$display("Data read. readFlag: %d, flag: %d",readFlag,flag);
		endmethod
	endmodule


	module mkTb();
		Corr_ifc x1 <- mkCorr;

		Reg#(Bit#(2)) i <- mkReg(0);
		Reg#(Int#(SumSize)) rg_sum <-mkReg(1);
		rule ra1 (i==0);
			x1.readData(1,2,3,4,5,6,7,8);
			//rg_sum <= x1.results();
			$display("rule1");
			i <= 1;
		endrule

		rule rl_disp_sum (i==1);
			let lv_sum= x1.results();
			$display("sum = %d",lv_sum);
			i <= 2;
		endrule
		rule ra2 (i==2);
			$display("rule2");
			x1.readData(-1,2,2,2,2,2,2,2);
			//rg_sum <= x1.results();
			i <= 3;
		endrule
		rule ra3 (i==3);
			let lv_sum = x1.results();
			$display("sum = %d",lv_sum);
			$finish(0);
		endrule
	endmodule // mkTb123
endpackage : correlator
