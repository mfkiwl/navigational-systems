package cordicRot;
	import initial_Settings::*;
	import Real::*;
	import FixedPoint::*;
	import Vector::*;
	import BRAMCore::*;

	interface CordicRotIfc;
		method FixedPoint#(2,32) cosine();
		method FixedPoint#(2,32) sine();
		method Action readIn(FixedPoint#(9,32) arg);
	endinterface

	module mkCordic(CordicRotIfc);
		Reg#(FixedPoint#(2,32)) k <- mkReg(0.6072529350088812561694);

		Reg#(FixedPoint#(2,32)) x <- mkReg(0);
		Reg#(FixedPoint#(2,32)) y <- mkReg(0);
		Reg#(FixedPoint#(9,32)) z <- mkReg(0);
		Reg#(Bit#(2)) d <- mkReg(1);

		Reg#(FixedPoint#(9,32)) phi <- mkReg(0.7854); 
		Reg#(FixedPoint#(2,32)) temp1 <- mkReg(0);
		Reg#(FixedPoint#(2,32)) temp2 <- mkReg(0);
		Reg#(FixedPoint#(9,32)) temp3 <- mkReg(0);

		Reg#(Bit#(2)) flag <- mkReg(-1);
		Reg#(Bit#(8)) count <- mkReg(0);

		rule rotate(count < 128 && flag == 0);
			$write("Rotate Iteration %d. x%d = ",count,count);
			fxptWrite(4,x);
			$write(", y%d = ",count);
			fxptWrite(4,y);
			$write(", z%d = ",count);
			fxptWrite(4,z);
			$display("");
			if(d == 1)
			begin
				temp1 <= x - (y >> count);
				temp2 <= y + (x >> count);
				temp3 <= z - phi;
			end
			else if(d == -1)
			begin
				temp1 <= x + (y >> count);
				temp2 <= y - (x >> count);
				temp3 <= z + phi;
			end
			flag <= 1;
		endrule

		rule store(count < 128 && flag == 1);
			$write("Store Iteration %d. x%d = ",count,count+1);
			fxptWrite(4,temp1);
			$write(", y%d = ",count+1);
			fxptWrite(4,temp2);
			$write(", z%d = ",count+1);
			fxptWrite(4,temp3);
			$display("\n");
			x <= temp1;
			y <= temp2;
			z <= temp3;
			if(temp3 < 0)
				d <= -1;
			else
				d <= 1;
			phi <= phi >> 1;
			count <= count+1;
			flag <= 0;

		endrule

		method FixedPoint#(2,32) cosine() if(count >= 128);
			return x;
		endmethod
		method FixedPoint#(2,32) sine() if(count >= 128);
			return y;
		endmethod
		method Action readIn(FixedPoint#(9,32) arg);
			z <= arg;
			x <= k;
			y <= 0;
			flag <= 0;
		endmethod

	endmodule // mkCordic


	module mkTb();
		CordicRotIfc ifc <- mkCordic;
		Reg#(Bit#(1)) flag <- mkReg(0);
		rule x1 (flag == 0);
			ifc.readIn(-0.523598776);
			flag <= 1;
		endrule
		rule x2	(flag == 1);
			let sin = ifc.sine();
			let cos = ifc.cosine();
			$write("Sine = ");
			fxptWrite(8,sin);
			$write("Cosine = ");
			fxptWrite(8,cos);
			$finish(0);
		endrule
	endmodule // mkTb
endpackage : cordicRot