package arctan;

    import Real ::*;
    import Vector ::*;

    interface Ifc_arctan;
        method Action get_inputs(Int#(32) i_in,Int#(32) q_in);
        method Int#(33) result_phase();
        method Int#(32) result_mag();
    endinterface : Ifc_arctan

    module mkarctan(Ifc_arctan);

        Reg#(Bit#(1)) readFlag <- mkReg(0);
        Reg#(Bit#(3)) flag <- mkReg(0);

        Reg#(Int#(32)) i <- mkReg(0);
        Reg#(Int#(32)) q <- mkReg(0);

        Vector#(31,Reg#(Int#(32))) atan_table <- replicateM(mkReg(0));
        Reg#(Int#(32)) magnitude <- mkReg(0);
        Reg#(Int#(32)) y <- mkReg(0);
        Reg#(Int#(32)) phase <- mkReg(0);
        Reg#(Int#(32)) temp1 <- mkReg(0);
        Reg#(Int#(32)) temp2 <- mkReg(0);

        Reg#(Bit#(6)) count <- mkReg(0);

        rule r1_converging_inputs(i<0);
            i<=-i;
            q<=-q;
        endrule
        rule r11_update_input((readFlag==1)&&(flag==0)&&(i>0));
            magnitude <= signExtend(i);
            y<=signExtend(q);
            flag <= 1;
        endrule
        rule r2_cordic_iteration_a((flag==1)&&(count<31));
            if(y < 0)
            begin
                temp1 <= magnitude - (y >> count);
                temp2 <= y + (magnitude >> count);
                phase <= phase - atan_table[count];
            end
            else
            begin
                temp1 <= magnitude + (y >> count);
                temp2 <= y - (magnitude >> count);
                phase <= phase + atan_table[count];
            end
            flag <= 2;
        endrule
        rule r3_cordic_iteration_b(flag==2);
            magnitude <= temp1;
            y <= temp2;
            //$display("\n Count = %d : magnitude = %d, phase = %d and y = %d",count,temp1,phase,temp2);
            count <= count+1;
            flag <= 1;
        endrule

        method Action get_inputs(Int#(32) i_in,Int#(32) q_in) if(readFlag == 0);
            atan_table[00] <= 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
            atan_table[01] <= 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
            atan_table[02] <= 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
            atan_table[03] <= 'b00000101000100010001000111010100; // atan(2^-3)
            atan_table[04] <= 'b00000010100010110000110101000011;
            atan_table[05] <= 'b00000001010001011101011111100001;
            atan_table[06] <= 'b00000000101000101111011000011110;
            atan_table[07] <= 'b00000000010100010111110001010101;
            atan_table[08] <= 'b00000000001010001011111001010011;
            atan_table[09] <= 'b00000000000101000101111100101110;
            atan_table[10] <= 'b00000000000010100010111110011000;
            atan_table[11] <= 'b00000000000001010001011111001100;
            atan_table[12] <= 'b00000000000000101000101111100110;
            atan_table[13] <= 'b00000000000000010100010111110011;
            atan_table[14] <= 'b00000000000000001010001011111001;
            atan_table[15] <= 'b00000000000000000101000101111100;
            atan_table[16] <= 'b00000000000000000010100010111110;
            atan_table[17] <= 'b00000000000000000001010001011111;
            atan_table[18] <= 'b00000000000000000000101000101111;
            atan_table[19] <= 'b00000000000000000000010100010111;
            atan_table[20] <= 'b00000000000000000000001010001011;
            atan_table[21] <= 'b00000000000000000000000101000101;
            atan_table[22] <= 'b00000000000000000000000010100010;
            atan_table[23] <= 'b00000000000000000000000001010001;
            atan_table[24] <= 'b00000000000000000000000000101000;
            atan_table[25] <= 'b00000000000000000000000000010100;
            atan_table[26] <= 'b00000000000000000000000000001010;
            atan_table[27] <= 'b00000000000000000000000000000101;
            atan_table[28] <= 'b00000000000000000000000000000010;
            atan_table[29] <= 'b00000000000000000000000000000001;
            atan_table[30] <= 'b00000000000000000000000000000000;
            i <= i_in;
            q <= q_in;
            readFlag <= 1;
        endmethod
        method Int#(33) result_phase() if(count==31);
            return zeroExtend(phase);
        endmethod
        method Int#(32) result_mag() if(count==31);
            return magnitude;
        endmethod
    endmodule : mkarctan
endpackage : arctan