import arctan::*;
import Real::*;
import Vector::*;

module mkTestbench();

    Ifc_arctan ifc_tb <- mkarctan;

    Reg#(Bit#(2)) flag <- mkReg(0);
    Reg#(Int#(32)) i <- mkReg(1610612736);
    Reg#(Int#(32)) q <- mkReg(923417968);
    Reg#(Int#(35)) magnitude <- mkReg(0);
    Reg#(Int#(32)) phase <- mkReg(0);

    rule r1(flag==0);
        ifc_tb.get_inputs(i,q);
        flag <= 1;
    endrule
    rule r2(flag==1);
        Int#(32) a = ifc_tb.result_phase();
        Int#(35) b = ifc_tb.result_mag();
        phase <= a;
        magnitude <= b;
        $display("\n For I_P = %d and Q_P = %d, atan(Q_P/I_P) = %d and magnitude = %d",i,q,a,b);
        $finish;
    endrule
endmodule : mkTestbench