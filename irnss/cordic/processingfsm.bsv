package processingfsm;

    import acquisitionfsm::*;
    import trackingfsm::*;
    import lockdetectorfsm::*;

    interface Ifc_processingfsm;
    endinterface : Ifc_processingfsm
    module mkprocessingfsm(Ifc_processingfsm);

        Ifc_acquisitionfsm ifc1 <- mkacquisitionfsm;
        Ifc_trackingfsm ifc2 <- mktrackingfsm;
        Ifc_lockdetectorfsm ifc3 <- mklockdetectorfsm;

        Reg#(Bit#(20)) rg_loopcnt <- mkReg(2);
        Reg#(FixedPoint#(16,32)) rg_carr_freq <- mkReg(0);
        Reg#(Bit#(22)) rg_code_shift <- mkReg(0);
        Reg#(Int#(33)) rg_last_phase <- mkReg(0);
        Reg#(Bit#(2)) rg_track_status <- mkReg(0);  
        Reg#(Bit#(1)) rg_sign_change <- mkReg(0);   

        Stmt processing_stmt = 
        (seq
            ifc1.getting_started();//Acquisition starts here
            Bit#(1) temp1 = ifc1.prn();
            if(temp1 == 1)//if the satellite is acquired
            seq
                action//Getting outputs of acquisition
                    rg_carr_freq <= fromInt(ifc1.freq_shifter);
                    rg_code_shift <= ifc1.code_shifter();
                    rg_last_phase <= 0;
                endaction
                while((rg_track_status != 3)&&(rg_loopcnt<=1500))
                seq
                    if(rg_track_status == 0)
                    begin
                        seq
                            ifc2.acq_inputs(rg_carr_freq,rg_carr_freq,rg_code_shift,rg_last_phase,rg_loopcnt);//Giving inputs to tracking iteration
                            action//Getting outputs of tracking iteration
                                rg_carr_freq <= ifc2.carr_freq_out();
                                rg_code_shift <= ifc2.code_shift_out();
                                rg_last_phase <= ifc2.phase_out();
                                rg_sign_change <= ifc2.sign_change;
                            endaction
                        endseq
                        ifc3.getinputs(rg_loopcnt,rg_sign_change);//Initialisation of Lockdetector FSM
                        rg_track_status <= ifc3.status();//Obtaining the status of tracking
                    end
                    else if(rg_track_status == 1)
                        seq
                            //HOT START IS PERFORMED
                        endseq
                    else if(rg_track_status == 2)
                    seq
                        //WARM START IS PERFORMED
                    endseq
                    if(rg_loopcnt%1500==0)
                    seq
                        //Read the 30s Navigation data from lockdetector FSM
                    endseq
                    rg_loopcnt <= rg_loopcnt + 1;
                endseq
                
            endseq
        endseq);

        FSM processing_fsm <- mkFSM(processing_stmt);

        rule r0_initialise(processing_fsm.done);
            processing_fsm.start();
        endrule

    endmodule : mkprocessingfsm
endpackage : processingfsm